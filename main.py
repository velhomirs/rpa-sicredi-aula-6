from desktopauto import DesktopTools
from fake_data import fake_data
from prefect import flow


@flow
def main():
    fake_data()
    DesktopTools.cadastra_contato()


if __name__ == '__main__':
    main()
